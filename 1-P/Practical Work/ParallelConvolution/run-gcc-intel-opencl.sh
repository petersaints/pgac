#!/bin/sh
NUM_RUNS=5
CONVOLUTION_TIME_LABEL="Convolution Time(ms) = "
RESULTS_FILE="results-gcc-intel-opencl.csv"
echo -n "" > "$RESULTS_FILE"
#CPU Integer version
for i in $(seq 1 1 $NUM_RUNS)
do
	RUN_TIME=$(./parallel-convolution-gcc-intel-opencl plane.ppm out.ppm matrix.txt 4 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
	echo $RUN_TIME
	echo -n $RUN_TIME"," >> "$RESULTS_FILE"
done
#CPU Float version
echo "" >> "$RESULTS_FILE"
for i in $(seq 1 1 $NUM_RUNS)
do
	RUN_TIME=$(./parallel-convolution-gcc-intel-opencl plane.ppm out.ppm matrix.txt 5 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
	echo $RUN_TIME
	echo -n $RUN_TIME"," >> "$RESULTS_FILE"
done
#GPU Integer version
echo "" >> "$RESULTS_FILE"
for i in $(seq 1 1 $NUM_RUNS)
do
	RUN_TIME=$(./parallel-convolution-gcc-intel-opencl plane.ppm out.ppm matrix.txt 8 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
	echo $RUN_TIME
	echo -n $RUN_TIME"," >> "$RESULTS_FILE"
done
#GPU Float version
echo "" >> "$RESULTS_FILE"
for i in $(seq 1 1 $NUM_RUNS)
do
	RUN_TIME=$(./parallel-convolution-gcc-intel-opencl plane.ppm out.ppm matrix.txt 9 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
	echo $RUN_TIME
	echo -n $RUN_TIME"," >> "$RESULTS_FILE"
done
