#!/bin/sh
source /opt/intel/bin/iccvars.sh intel64
source /opt/intel/bin/compilervars.sh intel64
#export SINK_LD_LIBRARY_PATH=/opt/intel/lib/mic/:/home/pesantos/ParallelConvolution/
#icc imglib.c main.c -mmic -std=c99 -march=native -O3 -lm -fopenmp -o parallel-convolution-icc-mic
icc imglib.c main.c -std=c99 -march=native -O3 -lm -fopenmp -o parallel-convolution-icc-mic
