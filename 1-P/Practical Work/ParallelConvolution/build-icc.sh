#!/bin/sh
source /opt/intel/bin/iccvars.sh intel64
source /opt/intel/bin/compilervars.sh intel64
icc imglib.c main.c -std=c99 -march=native -O3 -lm -fopenmp -o parallel-convolution-icc
