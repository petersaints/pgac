#include "imglib.h"
#define PPMREADBUFLEN 256
#define COLORS_PER_CHANNEL 256.0

#define float_free_grayimg(IMG) float_free_img((float_image)(IMG))
#define FLOAT_GET_PIXEL(IMG, X, Y) (IMG->buf[(((Y) * IMG->width + (X)))])

#define free_grayimg(IMG) free_img((image)(IMG))
#define GET_PIXEL(IMG, X, Y) (IMG->buf[((Y) * IMG->width + (X))])
/** FLOATING POINT TYPES AND FUNCTIONS (DIRECTLY PORTED FROM THE INTEGER VERSION, THUS NOT THOROUGHLY TESTED) **/
float_image float_alloc_img(unsigned int width, unsigned int height) {
    float_image img;
    img = (float_image) malloc(sizeof(float_image_t));
    img->buf = (float_pixel*) malloc(width*height*sizeof(float_pixel));
    img->width = width;
    img->height = height;
    return img;
}
void float_free_img(float_image img) {
    free(img->buf);
    free(img);
}
void float_fill_img(float_image img, float_color_component r, float_color_component g, float_color_component b) {
    unsigned int i, n;
    n = img->width * img->height;
    for (i=0; i < n; ++i) {
        img->buf[i][0] = r;
        img->buf[i][1] = g;
        img->buf[i][2] = b;
    }
}
void float_put_pixel_unsafe(float_image img, unsigned int x, unsigned int y, float_color_component r, float_color_component g, float_color_component b) {
    unsigned int ofs;
    ofs = (y * img->width) + x;
    img->buf[ofs][0] = r;
    img->buf[ofs][1] = g;
    img->buf[ofs][2] = b;
}
void float_put_pixel_clip(float_image img, unsigned int x, unsigned int y, float_color_component r, float_color_component g, float_color_component b) {
    if (x < img->width && y < img->height) float_put_pixel_unsafe(img, x, y, r, g, b);
}
float_grayimage float_alloc_grayimg(unsigned int width, unsigned int height) {
     float_grayimage img;
     img = (float_grayimage) malloc(sizeof(float_grayimage_t));
     img->buf = (float_pixel1*) malloc(width*height*sizeof(float_pixel1));
     img->width = width;
     img->height = height;
     return img;
}
float_grayimage float_tograyscale(float_image img) {
   unsigned int x, y;
   float_grayimage timg;
   float rc, gc, bc, l;
   unsigned int ofs;
 
   timg = float_alloc_grayimg(img->width, img->height);
   for(x=0; x < img->width; x++) {
      for(y=0; y < img->height; y++) {
        ofs = (y * img->width) + x;
        rc = (float) img->buf[ofs][0];
        gc = (float) img->buf[ofs][1];
        bc = (float) img->buf[ofs][2];
        l = 0.2126*rc + 0.7152*gc + 0.0722*bc;
        timg->buf[ofs][0] = (float_luminance) (l+0.5);
      }
   }
   return timg;
}
float_image float_tocolor(float_grayimage img) {
   unsigned int x, y;
   float_image timg;
   float_luminance l;
   unsigned int ofs;
 
   timg = float_alloc_img(img->width, img->height);
   for(x=0; x < img->width; x++) {
      for(y=0; y < img->height; y++) {
        ofs = (y * img->width) + x;
        l = img->buf[ofs][0];
        timg->buf[ofs][0] = l;
        timg->buf[ofs][1] = l;
        timg->buf[ofs][2] = l;
      }
   }
   return timg;
}
float_image float_get_ppm(FILE *pf) {
    image integerImage = get_ppm(pf);
    return integer_to_float_image(integerImage);
}

void float_output_ppm(FILE *fd, float_image floatImage) {
    output_ppm(fd, float_to_integer_image(floatImage));
}
/** INTEGER TYPES AND FUNCTIONS **/
image alloc_img(unsigned int width, unsigned int height) {
    image img;
    img = (image) malloc(sizeof(image_t));
    img->buf = (pixel*) malloc(width*height*sizeof(pixel));
    img->width = width;
    img->height = height;
    return img;
}
void free_img(image img) {
    free(img->buf);
    free(img);
}
void fill_img(image img, color_component r, color_component g, color_component b) {
    unsigned int i, n;
    n = img->width * img->height;
    for (i=0; i < n; ++i) {
        img->buf[i][0] = r;
        img->buf[i][1] = g;
        img->buf[i][2] = b;
    }
}
void put_pixel_unsafe(image img, unsigned int x, unsigned int y, color_component r, color_component g, color_component b) {
    unsigned int ofs;
    ofs = (y * img->width) + x;
    img->buf[ofs][0] = r;
    img->buf[ofs][1] = g;
    img->buf[ofs][2] = b;
}
void put_pixel_clip(image img, unsigned int x, unsigned int y, color_component r, color_component g, color_component b) {
    if (x < img->width && y < img->height) put_pixel_unsafe(img, x, y, r, g, b);
}
grayimage alloc_grayimg(unsigned int width, unsigned int height) {
     grayimage img;
     img = (grayimage) malloc(sizeof(grayimage_t));
     img->buf = (pixel1*) malloc(width*height*sizeof(pixel1));
     img->width = width;
     img->height = height;
     return img;
}
grayimage tograyscale(image img) {
   unsigned int x, y;
   grayimage timg;
   float rc, gc, bc, l;
   unsigned int ofs;
 
   timg = alloc_grayimg(img->width, img->height);
   for(x=0; x < img->width; x++) {
      for(y=0; y < img->height; y++) {
        ofs = (y * img->width) + x;
        rc = (float) img->buf[ofs][0];
        gc = (float) img->buf[ofs][1];
        bc = (float) img->buf[ofs][2];
        l = 0.2126*rc + 0.7152*gc + 0.0722*bc;
        timg->buf[ofs][0] = (luminance) (l+0.5);
      }
   }
   return timg;
}
image tocolor(grayimage img) {
   unsigned int x, y;
   image timg;
   luminance l;
   unsigned int ofs;
 
   timg = alloc_img(img->width, img->height);
   for(x=0; x < img->width; x++) {
      for(y=0; y < img->height; y++) {
        ofs = (y * img->width) + x;
        l = img->buf[ofs][0];
        timg->buf[ofs][0] = l;
        timg->buf[ofs][1] = l;
        timg->buf[ofs][2] = l;
      }
   }
   return timg;
}
image get_ppm(FILE *pf) {
        char buf[PPMREADBUFLEN], *t;
        image img;
        unsigned int w, h, d;
        int r;
 
        if (pf == NULL) return NULL;
        t = fgets(buf, PPMREADBUFLEN, pf);
        /* the code fails if the white space following "P6" is not '\n' */
        if ( (t == NULL) || ( strncmp(buf, "P6\n", 3) != 0 ) ) return NULL;
        do
        { /* Px formats can have # comments after first line */
           t = fgets(buf, PPMREADBUFLEN, pf);
           if ( t == NULL ) return NULL;
        } while ( strncmp(buf, "#", 1) == 0 );
        r = sscanf(buf, "%u %u", &w, &h);
        if ( r < 2 ) return NULL;
 
        r = fscanf(pf, "%u", &d);
        if ( (r < 1) || ( d != 255 ) ) return NULL;
        fseek(pf, 1, SEEK_CUR); /* skip one byte, should be whitespace */
 
        img = alloc_img(w, h);
        if ( img != NULL )
        {
            size_t rd = fread(img->buf, sizeof(pixel), w*h, pf);
            if ( rd < w*h )
            {
               free_img(img);
               return NULL;
            }
            return img;
        }
	return NULL;
}
void output_ppm(FILE *fd, image img) {
  unsigned int n;
  (void) fprintf(fd, "P6\n%d %d\n255\n", img->width, img->height);
  n = img->width * img->height;
  (void) fwrite(img->buf, sizeof(pixel), n, fd);
  (void) fflush(fd);
}
/** INTEGER/FLOAT CONVERSION **/
float_image integer_to_float_image(image integerImage) {
    float_image floatImage = float_alloc_img(integerImage->width,integerImage->height);
    integer_to_float_image_in_out(integerImage, floatImage);
    return floatImage;
}
image float_to_integer_image(float_image floatImage) {
    image integerImage = alloc_img(floatImage->width,floatImage->height);
    float_to_integer_image_in_out(floatImage, integerImage);
    return integerImage;
}
void float_to_integer_image_in_out(float_image floatImage, image integerImage) {
    for(int i = 0; i < integerImage->height; i++) {
        for(int j = 0; j < integerImage->width; j++) {
            float* p= FLOAT_GET_PIXEL(floatImage,j,i);
            put_pixel_unsafe(integerImage,j,i,p[0]*COLORS_PER_CHANNEL,p[1]*COLORS_PER_CHANNEL,p[2]*COLORS_PER_CHANNEL);
        }
    }
}
void integer_to_float_image_in_out(image integerImage, float_image floatImage) {
    for(int i = 0; i < integerImage->height; i++) {
        for(int j = 0; j < integerImage->width; j++) {
            unsigned char* p= GET_PIXEL(integerImage,j,i);
            float_put_pixel_unsafe(floatImage,j,i,p[0]/COLORS_PER_CHANNEL,p[1]/COLORS_PER_CHANNEL,p[2]/COLORS_PER_CHANNEL);
        }
    }
}
