#define _GNU_SOURCE
#include "imglib.h"
#include <stdbool.h>
#include <sys/time.h>
#include <omp.h>

#define DIMENSIONS				2 //0 --> Height (Rows) / 1 --> Height (Columns)
#define CHANNELS 				3 

#define OPENMP_DEFAULT_SCHEDULE 		-1
#define OPENMP_AUTO_SCHEDULE 			0
#define OPENMP_STATIC_SCHEDULE 			1
#define OPENMP_DYNAMIC_SCHEDULE 		2
#define OPENMP_GUIDED_SCHEDULE 			3

#define CONVOLUTION_OPENMP 			0
#define CONVOLUTION_OPENMP_FLOAT 		1
#define CONVOLUTION_OPENMP_ACCELERATOR 		2
#define CONVOLUTION_OPENMP_ACCELERATOR_FLOAT 	3
#define CONVOLUTION_OPENCL_CPU 			4
#define CONVOLUTION_OPENCL_CPU_FLOAT 		5
#define CONVOLUTION_OPENCL_GPU 			6
#define CONVOLUTION_OPENCL_GPU_FLOAT 		7
#define CONVOLUTION_OPENCL_ACCELERATOR 		8
#define CONVOLUTION_OPENCL_ACCELERATOR_FLOAT 	9

#ifdef OPENCL_SUPPORT
    #include <CL/cl.h>
    #define STRING_BUFFER_SIZE 4096
    void checkOpenCLError(cl_int error) {
        if(error != CL_SUCCESS) {
            printf("OpenCL Error with Code: %d\n", error);
            exit(EXIT_FAILURE);
        }
    }
    void printOpenCLDeviceInfo(cl_device_id device) {
        char deviceInfoBuffer[STRING_BUFFER_SIZE];
        cl_uint deviceInfoBufferUint;
        cl_ulong deviceInfoBufferUlong;
        size_t deviceInfoBufferSizeT;
        size_t deviceInfoBufferSizeTp[3];

        printf("--- DEVICE INFORMATION ---\n");
        clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(deviceInfoBuffer), deviceInfoBuffer, NULL);
        printf("DEVICE_NAME = %s\n", deviceInfoBuffer);
        
        clGetDeviceInfo(device, CL_DEVICE_VENDOR, sizeof(deviceInfoBuffer), deviceInfoBuffer, NULL);
        printf("DEVICE_VENDOR = %s\n", deviceInfoBuffer);
        
        clGetDeviceInfo(device, CL_DEVICE_VERSION, sizeof(deviceInfoBuffer), deviceInfoBuffer, NULL);
        printf("DEVICE_VERSION = %s\n", deviceInfoBuffer);
        
        clGetDeviceInfo(device, CL_DRIVER_VERSION, sizeof(deviceInfoBuffer), deviceInfoBuffer, NULL);
        printf("DRIVER_VERSION = %s\n", deviceInfoBuffer);
        
        clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(deviceInfoBufferUint), &deviceInfoBufferUint, NULL);
        printf("DEVICE_MAX_COMPUTE_UNITS = %u\n", (unsigned int)deviceInfoBufferUint);
        
        clGetDeviceInfo(device, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(deviceInfoBufferUint), &deviceInfoBufferUint, NULL);
        printf("DEVICE_MAX_CLOCK_FREQUENCY = %u\n", (unsigned int)deviceInfoBufferUint);
        
        clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(deviceInfoBufferUlong), &deviceInfoBufferUlong, NULL);
        printf("DEVICE_GLOBAL_MEM_SIZE = %llu\n", (unsigned long long)deviceInfoBufferUlong);
                        
        clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(deviceInfoBufferUlong), &deviceInfoBufferUlong, NULL);
        printf("DEVICE_GLOBAL_MEM_CACHE_SIZE = %llu\n", (unsigned long long)deviceInfoBufferUlong);
        
        clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, sizeof(deviceInfoBufferUint), &deviceInfoBufferUint, NULL);
        printf("DEVICE_GLOBAL_MEM_CACHELINE_SIZE = %u\n", (unsigned int)deviceInfoBufferUint);
        
        clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(deviceInfoBufferUlong), &deviceInfoBufferUlong, NULL);
        printf("DEVICE_LOCAL_MEM_SIZE = %llu\n", (unsigned long long)deviceInfoBufferUlong);
        
        clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(deviceInfoBufferSizeT), &deviceInfoBufferSizeT, NULL);
        printf("DEVICE_MAX_WORK_GROUP_SIZE = %lu\n", deviceInfoBufferSizeT);
        
        clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(deviceInfoBufferSizeTp), &deviceInfoBufferSizeTp, NULL);
        printf("DEVICE_MAX_WORK_ITEM_SIZES = %lu, %lu, %lu\n", deviceInfoBufferSizeTp[0], deviceInfoBufferSizeTp[1], deviceInfoBufferSizeTp[2]);        
        printf("------------\n");
    }
    void buildOpenCLProgram(cl_device_id device, cl_program program) {
        cl_int error;
        error=clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
        if (error != CL_SUCCESS) {
            char buildLog[STRING_BUFFER_SIZE];
            clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, sizeof(buildLog), buildLog, NULL);
            printf("Error in kernel:\n%s", buildLog);
            clReleaseProgram(program);
            exit(EXIT_FAILURE);
        }
    }
    cl_program createOpenCLProgram(cl_context context, cl_device_id device, const char* fileName)
    {
        cl_program program;
        cl_int error;
        FILE *fp=fopen(fileName,"rb");
        if(fp == NULL) {
            printf("Failed to OpenCL source code file.\n");
            return NULL;
        }
        // Determine the size of the binary
        size_t binarySize;
        fseek(fp, 0, SEEK_END);
        binarySize = ftell(fp);
        rewind(fp);
        
        char* src = malloc(binarySize * sizeof(char));
        size_t sourceSize = fread(src, binarySize, sizeof(char), fp);
        fclose(fp);
        
        const char *srcptr[]={src};
        program = clCreateProgramWithSource(context, 1, srcptr, &binarySize, &error);
        checkOpenCLError(error);
        if (program == NULL) {
            printf("Failed to create CL program from source.\n");
            return NULL;
        }
        buildOpenCLProgram(device, program);
        return program;
    }    
#endif
void convolve2DOpenMP(image in, image out, int dataSizeX, int dataSizeY, float* kernel, int kernelSizeX, int kernelSizeY) {
    //find center position of kernel (half of kernel size)
    const int kCenterX = kernelSizeX / 2;
    const int kCenterY = kernelSizeY / 2;
    #pragma omp parallel for
    for(int i=0; i < dataSizeY; i++) {                  //rows
        for(int j=0; j < dataSizeX; j++) {              //columns
            float_pixel sum = {};
            for(int m=0; m < kernelSizeY; m++) {        //kernel rows
                int mm = kernelSizeY - 1 - m;           //row index of flipped kernel
                for(int n=0; n < kernelSizeX; n++) {    //kernel columns
                    int nn = kernelSizeX - 1 - n;       //column index of flipped kernel
                    //index of input signal, used for checking boundary
                    int rowIndex = i + m - kCenterY;
                    int colIndex = j + n - kCenterX;
                    //ignore input samples which are out of bound
                    if(rowIndex >= 0 && rowIndex < dataSizeY && colIndex >= 0 && colIndex < dataSizeX) {
                        int inOffset = dataSizeX * rowIndex + colIndex;
                        int kernelOffset = kernelSizeX * mm + nn;
                        for(int c = 0; c < CHANNELS; c++) {
                            sum[c] += in->buf[inOffset][c] * kernel[kernelOffset];
                        }             
                    }
                }
            }
            int outOffset = dataSizeX * i + j;
            for(int c = 0; c < CHANNELS; c++) {
                 out->buf[outOffset][c] = sum[c];
            }
        }
    }
}
void convolve2DOpenMPFloat(float_image in, float_image out, int dataSizeX, int dataSizeY, float* kernel, int kernelSizeX, int kernelSizeY) {
    //find center position of kernel (half of kernel size)
    const int kCenterX = kernelSizeX / 2;
    const int kCenterY = kernelSizeY / 2;
    #pragma omp parallel for
    for(int i=0; i < dataSizeY; i++) {                  //rows
        for(int j=0; j < dataSizeX; j++) {              //columns
            float_pixel sum = {};
            for(int m=0; m < kernelSizeY; m++) {        //kernel rows
                int mm = kernelSizeY - 1 - m;           //row index of flipped kernel
                for(int n=0; n < kernelSizeX; n++) {    //kernel columns
                    int nn = kernelSizeX - 1 - n;       //column index of flipped kernel
                    //index of input signal, used for checking boundary
                    int rowIndex = i + m - kCenterY;
                    int colIndex = j + n - kCenterX;
                    //ignore input samples which are out of bound
                    if(rowIndex >= 0 && rowIndex < dataSizeY && colIndex >= 0 && colIndex < dataSizeX) {
                        int inOffset = dataSizeX * rowIndex + colIndex;
                        int kernelOffset = kernelSizeX * mm + nn;
                        for(int c = 0; c < CHANNELS; c++) {
                            sum[c] += in->buf[inOffset][c] * kernel[kernelOffset];
                        }             
                    }
                }
            }
            int outOffset = dataSizeX * i + j;
            memcpy(out->buf[outOffset], sum, CHANNELS*sizeof(float));
        }
    }
}
void convolve2DOpenMPAccelerator(unsigned char *in, unsigned char *out, int dataSizeX, int dataSizeY, float* kernel, int kernelSizeX, int kernelSizeY) {
    //find center position of kernel (half of kernel size)
    const int kCenterX = kernelSizeX / 2;
    const int kCenterY = kernelSizeY / 2;
    const int imageSize = dataSizeX * dataSizeY * CHANNELS;
    const int kernelSize = kernelSizeX * kernelSizeY;
    #pragma offload target (mic:0) in(in : length(imageSize)) out(out : length (imageSize)) in(kernel : length(kernelSize))
    #pragma omp parallel for
    for(int i=0; i < dataSizeY; i++) {                  //rows
        for(int j=0; j < dataSizeX; j++) {              //columns
            float sum[3] = {};
            for(int m=0; m < kernelSizeY; m++) {        //kernel rows
                int mm = kernelSizeY - 1 - m;           //row index of flipped kernel
                for(int n=0; n < kernelSizeX; n++) {    //kernel columns
                    int nn = kernelSizeX - 1 - n;       //column index of flipped kernel
                    //index of input signal, used for checking boundary
                    int rowIndex = i + m - kCenterY;
                    int colIndex = j + n - kCenterX;
                    //ignore input samples which are out of bound
                    if(rowIndex >= 0 && rowIndex < dataSizeY && colIndex >= 0 && colIndex < dataSizeX) {
                        int inOffset = (dataSizeX * rowIndex + colIndex) * CHANNELS;
                        int kernelOffset = kernelSizeX * mm + nn;
                        for(int c = 0; c < CHANNELS; c++) {
                            sum[c] += in[inOffset+c] * kernel[kernelOffset];
                        }             
                    }
                }
            }
            int outOffset = (dataSizeX * i + j) * CHANNELS;
            for(int c = 0; c < CHANNELS; c++) {
                 out[outOffset+c] = sum[c];
            }
        }
    }
}
void convolve2DOpenMPFloatAccelerator(float *in, float *out, int dataSizeX, int dataSizeY, float* kernel, int kernelSizeX, int kernelSizeY) {
    //find center position of kernel (half of kernel size)
    const int kCenterX = kernelSizeX / 2;
    const int kCenterY = kernelSizeY / 2;
    const int imageSize = dataSizeX * dataSizeY * CHANNELS;
    const int kernelSize = kernelSizeX*kernelSizeY;
    #pragma offload target (mic:0) in(in : length(imageSize)) out(out : length (imageSize)) in(kernel : length(kernelSize))
    #pragma omp parallel for
    for(int i=0; i < dataSizeY; i++) {                  //rows
        for(int j=0; j < dataSizeX; j++) {              //columns
            float sum[3] = {};
            for(int m=0; m < kernelSizeY; m++) {        //kernel rows
                int mm = kernelSizeY - 1 - m;           //row index of flipped kernel
                for(int n=0; n < kernelSizeX; n++) {    //kernel columns
                    int nn = kernelSizeX - 1 - n;       //column index of flipped kernel
                    //index of input signal, used for checking boundary
                    int rowIndex = i + m - kCenterY;
                    int colIndex = j + n - kCenterX;
                    //ignore input samples which are out of bound
                    if(rowIndex >= 0 && rowIndex < dataSizeY && colIndex >= 0 && colIndex < dataSizeX) {
                        int inOffset = (dataSizeX * rowIndex + colIndex) * CHANNELS;
                        int kernelOffset = kernelSizeX * mm + nn;
                        for(int c = 0; c < CHANNELS; c++) {
                            sum[c] += in[inOffset+c] * kernel[kernelOffset];
                        }             
                    }
                }
            }
            int outOffset = (dataSizeX * i + j) * CHANNELS;
            memcpy(&out[outOffset], sum, CHANNELS*sizeof(float));
        }
    }
}
int main(int argc, char** argv) {
    if(argc < 5 || argc > 8) {
        printf("Usage: %s input_path output_path kernel_path convolution_mode [n_threads/device_id] [schedule_mode] [chunk_size]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    int convolutionMode = atoi(argv[4]);    
    int numThreads = 0;
    if(argc >= 6) numThreads = atoi(argv[5]);
    
    int openMpSchedule = OPENMP_DEFAULT_SCHEDULE;
    if(argc >= 7) openMpSchedule = atoi(argv[6]);
    
    int chunkSize = 0;
    if(argc >= 8) chunkSize = atoi(argv[7]);
    
    //Input Parameters
    printf("Input Path: %s\nOutput Path: %s\nKernel Path: %s\nConvolution Mode: %d \nNumber of Threads: %d\n", argv[1], argv[2], argv[3], convolutionMode, numThreads);
    
    FILE *kernelFile;
    char *line = NULL;
    char *tok = NULL;
    size_t len = 0;
    ssize_t read;
    int kernelDimensions[DIMENSIONS];
    
    //--- READ KERNEL MATRIX DIMENSIONS ---
    kernelFile = fopen(argv[3], "r");
    if (kernelFile == NULL)
        exit(EXIT_FAILURE);
    if((read = getline(&line, &len, kernelFile)) != -1) {
        tok = strtok(line, ",");
        for(int i = 0; tok; i++) {
           kernelDimensions[i] = atoi(tok); 
           tok = strtok(NULL, ",");
        }
    }

    //--- KERNEL ALLOCATION ---
    int kernelSize = 1;
    for(int i = 0; i < DIMENSIONS; i++) {
        kernelSize*=kernelDimensions[i];
    }
    float *kernel = malloc(kernelSize*sizeof(float));
    if (kernel == NULL)
        exit(EXIT_FAILURE);
    
    for(int i = 0; i < kernelDimensions[0]
    && (read = getline(&line, &len, kernelFile)) != -1; i++) {
        tok = strtok(line, "\t");
        for(int j = 0; j < kernelDimensions[1] && tok; j++) {
            kernel[i*kernelDimensions[1]+j] = atof(tok);
            tok = strtok(NULL, "\t");
        }
    }
    free(line);
    
    //--- PRINT KERNEL ---
    for(int i = 0; i < kernelSize; i++) {
        if(i != 0 && i % kernelDimensions[1] == 0)        
            printf("\n");
        printf("%f ",kernel[i]);
        if(i == kernelSize-1)
            printf("\n");
    }

    FILE *inFile = fopen(argv[1], "r");
    image input = get_ppm(inFile);
    image output = alloc_img(input->width, input->height);
    fclose(inFile);
    
    if(convolutionMode == CONVOLUTION_OPENMP
    || convolutionMode == CONVOLUTION_OPENMP_FLOAT) {
        printf("Using OpenMP with %d threads\n", numThreads);
        omp_set_num_threads(numThreads);
        switch(openMpSchedule) {
            case OPENMP_AUTO_SCHEDULE:
                printf("Using OpenMP Auto Schedule with Chunk Size: %d\n", chunkSize);
                omp_set_schedule(omp_sched_auto, chunkSize);
                break;
            case OPENMP_STATIC_SCHEDULE:
                printf("Using OpenMP Static Schedule with Chunk Size: %d\n", chunkSize);
                omp_set_schedule(omp_sched_static, chunkSize);
                break;
            case OPENMP_DYNAMIC_SCHEDULE:
                printf("Using OpenMP Dynamic Schedule with Chunk Size: %d\n", chunkSize);
                omp_set_schedule(omp_sched_dynamic, chunkSize);
                break;
            case OPENMP_GUIDED_SCHEDULE:
                printf("Using OpenMP Guided Schedule with Chunk Size: %d\n", chunkSize);
                omp_set_schedule(omp_sched_guided, chunkSize);
                break;
            default:
                printf("Using OpenMP Default Schedule\n");
                break;
        }
    }
    struct timeval startCompute,endCompute;
    printf("Starting Convolution Computation!\n");
    gettimeofday(&startCompute, NULL);
    switch(convolutionMode) {
        case CONVOLUTION_OPENMP:
        default: {
                printf("Using Convolve2D Algorithm on the CPU using OpenMP\n");
                convolve2DOpenMP(input, output, input->width, input->height, kernel, kernelDimensions[0], kernelDimensions[1]);
            }
            break;
        case CONVOLUTION_OPENMP_FLOAT: {
                printf("Using Convolve2D Algorithm on the CPU using OpenMP (Float)\n");
                float_image inputFloat = integer_to_float_image(input);
                float_image outputFloat = float_alloc_img(output->width,output->height);

	        struct timeval begin,end;
	        gettimeofday(&begin, NULL);

                convolve2DOpenMPFloat(inputFloat, outputFloat, inputFloat->width, inputFloat->height, kernel, kernelDimensions[0], kernelDimensions[1]); 

		gettimeofday(&end, NULL);
		printf("Compute Time Alone(ms) = %ld\n", (end.tv_sec - begin.tv_sec)*1000 + (end.tv_usec - begin.tv_usec)/1000);
           
                float_to_integer_image_in_out(outputFloat, output);
                float_free_img(inputFloat);
                float_free_img(outputFloat);
            }
            break;
	case CONVOLUTION_OPENMP_ACCELERATOR: {
                printf("Using Convolve2D Algorithm on the CPU using OpenMP Accelerator\n");
                convolve2DOpenMPAccelerator((unsigned char *)input->buf, (unsigned char *)output->buf, input->width, input->height, kernel, kernelDimensions[0], kernelDimensions[1]);
            }
            break;
        case CONVOLUTION_OPENMP_ACCELERATOR_FLOAT: {
                printf("Using Convolve2D Algorithm on the CPU using OpenMP Accelerator (Float)\n");
                float_image inputFloat = integer_to_float_image(input);
                float_image outputFloat = float_alloc_img(output->width,output->height);

	        struct timeval begin,end;
	        gettimeofday(&begin, NULL);

                convolve2DOpenMPFloatAccelerator((float *)inputFloat->buf, (float *)outputFloat->buf, inputFloat->width, inputFloat->height, kernel, kernelDimensions[0], kernelDimensions[1]);            
		gettimeofday(&end, NULL);
		printf("Compute Time Alone(ms) = %ld\n", (end.tv_sec - begin.tv_sec)*1000 + (end.tv_usec - begin.tv_usec)/1000);
		   
                float_to_integer_image_in_out(outputFloat, output);
                float_free_img(inputFloat);
                float_free_img(outputFloat);
            }
            break;
        case CONVOLUTION_OPENCL_CPU:
        case CONVOLUTION_OPENCL_CPU_FLOAT:
        case CONVOLUTION_OPENCL_GPU:
        case CONVOLUTION_OPENCL_GPU_FLOAT:
        case CONVOLUTION_OPENCL_ACCELERATOR:
        case CONVOLUTION_OPENCL_ACCELERATOR_FLOAT:{
                printf("Using Convolve2D Algorithm with OpenCL\n");
                #ifdef OPENCL_SUPPORT
                printf("OpenCL is enabled/supported\n");
                
                cl_int error;
                cl_platform_id platform;
                cl_device_id device;
		cl_device_id *devices;
                cl_device_type devtype = CL_DEVICE_TYPE_ALL;
                cl_uint platforms, numDevices;     
                error=clGetPlatformIDs(1, &platform, &platforms);
                checkOpenCLError(error);
                
                if(convolutionMode == CONVOLUTION_OPENCL_CPU
                || convolutionMode == CONVOLUTION_OPENCL_CPU_FLOAT) {
                    devtype = CL_DEVICE_TYPE_CPU;
                    printf("The CPU will be used\n");
                } else if(convolutionMode == CONVOLUTION_OPENCL_GPU
                       || convolutionMode == CONVOLUTION_OPENCL_GPU_FLOAT) {
                    devtype = CL_DEVICE_TYPE_GPU;
                    printf("The GPU will be used\n");
                } else if(convolutionMode == CONVOLUTION_OPENCL_ACCELERATOR
                       || convolutionMode == CONVOLUTION_OPENCL_ACCELERATOR_FLOAT) {
                    devtype = CL_DEVICE_TYPE_ACCELERATOR;
                    printf("An accelerator will be used\n");
                } else {
                    printf("Wrong device type specified. Using any device available\n");
                    devtype = CL_DEVICE_TYPE_ALL;
                }
                
   		cl_uint deviceCount;
		error=clGetDeviceIDs(platform, devtype, 0, NULL, &numDevices);
		checkOpenCLError(error);
		printf("Number of Devices Found: %d\n", numDevices);
		
		devices = malloc(numDevices*sizeof(cl_device_id));     		
                error=clGetDeviceIDs(platform, devtype, numDevices, devices, NULL);
                checkOpenCLError(error);

		printf("Selecting Device Number: %d\n", numThreads);
		device = devices[numThreads];
		free(devices);                

                cl_context_properties properties[]={CL_CONTEXT_PLATFORM, (cl_context_properties)platform,0};
                cl_context context=clCreateContext(properties, 1, &device, NULL, NULL, &error);
                checkOpenCLError(error);
                
                cl_command_queue cq = clCreateCommandQueue(context, device, 0, &error);
                checkOpenCLError(error);

                printOpenCLDeviceInfo(device);

                cl_program prog;
                cl_kernel kernelConvolution;
               
                cl_mem inputImageBuffer, kernelFilter, workingImageBuffer;
                float_image inputFloat, outputFloat;
                
                size_t worksize=input->width*input->height*CHANNELS;
                if(convolutionMode == CONVOLUTION_OPENCL_CPU
                || convolutionMode == CONVOLUTION_OPENCL_GPU
                || convolutionMode == CONVOLUTION_OPENCL_ACCELERATOR) {
                    printf("Using integer-based kernel\n");
                    prog = createOpenCLProgram(context,device, "parallel-convolution.cl");
                    kernelConvolution = clCreateKernel(prog, "parallelconvolution", &error);
                    checkOpenCLError(error);
                    
                    inputImageBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, worksize, input->buf, &error);
                    checkOpenCLError(error);
                    
                    kernelFilter = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, kernelSize*sizeof(float), kernel, &error);
                    checkOpenCLError(error);
                    
                    workingImageBuffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, worksize, NULL, &error);
                    checkOpenCLError(error);
                } else if(convolutionMode == CONVOLUTION_OPENCL_CPU_FLOAT
                       || convolutionMode == CONVOLUTION_OPENCL_GPU_FLOAT
                       || convolutionMode == CONVOLUTION_OPENCL_ACCELERATOR_FLOAT) {
                    printf("Using float-based kernel\n");
                    prog = createOpenCLProgram(context,device, "parallel-convolution-float.cl");
                    kernelConvolution = clCreateKernel(prog, "parallelconvolution", &error);
                    checkOpenCLError(error);
                    
                    inputFloat = integer_to_float_image(input);
                    
                    inputImageBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, worksize*sizeof(float), inputFloat->buf, &error);
                    checkOpenCLError(error);
                    
                    kernelFilter = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, kernelSize*sizeof(float), kernel, &error);
                    checkOpenCLError(error);
                    
                    workingImageBuffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, worksize*sizeof(float), NULL, &error);
                    checkOpenCLError(error);
                } else {
                    printf("Unexpected Error!");
                    exit(EXIT_FAILURE);
                }
                clSetKernelArg(kernelConvolution, 0, sizeof(inputImageBuffer), &inputImageBuffer);
                clSetKernelArg(kernelConvolution, 1, sizeof(workingImageBuffer), &workingImageBuffer);
                clSetKernelArg(kernelConvolution, 2, sizeof(kernelFilter), &kernelFilter);
                
                cl_uint2 inputImageSize = {input->width, input->height};
                clSetKernelArg(kernelConvolution, 3, sizeof(cl_uint2), &inputImageSize);
                
                cl_uint2 kernelFilterSize = {kernelDimensions[1], kernelDimensions[0]};
                clSetKernelArg(kernelConvolution, 4, sizeof(cl_uint2), &kernelFilterSize);

                size_t wsize[2] = {input->width, input->height};
                cl_uint dimensions = 2;
                //Perform the operation
                error=clEnqueueNDRangeKernel(cq, kernelConvolution, dimensions, NULL, wsize, NULL, 0, NULL, NULL);
                checkOpenCLError(error);
                
                if(convolutionMode == CONVOLUTION_OPENCL_CPU
                || convolutionMode == CONVOLUTION_OPENCL_GPU
                || convolutionMode == CONVOLUTION_OPENCL_ACCELERATOR) {
                    error=clEnqueueReadBuffer(cq, workingImageBuffer, CL_FALSE, 0, worksize, output->buf, 0, NULL, NULL);
                    checkOpenCLError(error);
                    
                    //Await completion of all the above
                    error=clFinish(cq);
                    checkOpenCLError(error);
                } else if(convolutionMode == CONVOLUTION_OPENCL_CPU_FLOAT
                       || convolutionMode == CONVOLUTION_OPENCL_GPU_FLOAT
                       || convolutionMode == CONVOLUTION_OPENCL_ACCELERATOR_FLOAT) {
                    outputFloat = float_alloc_img(output->width,output->height);

                    error=clEnqueueReadBuffer(cq, workingImageBuffer, CL_FALSE, 0, worksize*sizeof(float), outputFloat->buf, 0, NULL, NULL);
                    checkOpenCLError(error);
                    
                    //Await completion of all the above
                    error=clFinish(cq);
                    checkOpenCLError(error);
                
                    float_to_integer_image_in_out(outputFloat, output);
                    float_free_img(inputFloat);
                    float_free_img(outputFloat);
                } else {
                    printf("Unexpected Error!");
                    exit(EXIT_FAILURE);
                }

                if(cq != NULL) clReleaseCommandQueue(cq);
                if(kernelConvolution != NULL) clReleaseKernel(kernelConvolution);
                if(prog != NULL) clReleaseProgram(prog);
                if(context != NULL) clReleaseContext(context);
                if(inputImageBuffer != NULL) clReleaseMemObject(inputImageBuffer);
                if(kernelFilter != NULL) clReleaseMemObject(kernelFilter);
                if(workingImageBuffer != NULL) clReleaseMemObject(workingImageBuffer);
                #else
                printf("OpenCL is NOT enabled/supported so the program will abort\n");
                exit(EXIT_FAILURE);
                #endif
            }
            break;
    }    
    gettimeofday(&endCompute, NULL);
    printf("Convolution Time(ms) = %ld\n", (endCompute.tv_sec - startCompute.tv_sec)*1000 + (endCompute.tv_usec - startCompute.tv_usec)/1000);
    
    FILE *outFile = fopen(argv[2], "w+");
    printf("Saving Output... \n");
    output_ppm(outFile, output);
    fclose(outFile);
    
    free_img(input);
    free_img(output);
    free(kernel);
    
    exit(EXIT_SUCCESS);
}

