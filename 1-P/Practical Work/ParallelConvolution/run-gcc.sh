#!/bin/sh
NUM_CORES=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || sysctl -n hw.ncpu)
NUM_RUNS=5
CONVOLUTION_TIME_LABEL="Convolution Time(ms) = "
RESULTS_FILE="results-gcc.csv"
echo -n "" > "$RESULTS_FILE"
#Integer version
for i in $(seq 1 1 $NUM_CORES)
do
	for j in $(seq 1 1 $NUM_RUNS)
	do
		RUN_TIME=$(./parallel-convolution-gcc plane.ppm out.ppm matrix.txt 0 $i | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
		echo $RUN_TIME
		echo -n $RUN_TIME"," >> "$RESULTS_FILE"
	done
done
#Float version
echo "" >> "$RESULTS_FILE"
for i in $(seq 1 1 $NUM_CORES)
do
	for j in $(seq 1 1 $NUM_RUNS)
	do
		RUN_TIME=$(./parallel-convolution-gcc plane.ppm out.ppm matrix.txt 1 $i | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
		echo $RUN_TIME
		echo -n $RUN_TIME"," >> "$RESULTS_FILE"
	done
done