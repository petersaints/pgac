#ifndef _IMGLIB_0
#define _IMGLIB_0
 
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <math.h>
#include <sys/queue.h>


/** FLOATING POINT TYPES AND FUNCTIONS (DIRECTLY PORTED FROM THE INTEGER VERSION, THUS NOT THOROUGHLY TESTED) **/
typedef float float_color_component;
typedef float_color_component float_pixel[3];
typedef struct {
    unsigned int width;
    unsigned int height;
    float_pixel *buf;
} float_image_t;
typedef float_image_t *float_image;

typedef float float_luminance;
typedef float_luminance float_pixel1[1];
typedef struct {
   unsigned int width;
   unsigned int height;
   float_pixel1 *buf;
} float_grayimage_t;
typedef float_grayimage_t *float_grayimage;

float_image float_alloc_img(unsigned int width, unsigned int height);
void float_free_img(float_image);

void float_fill_img(float_image img,
                    float_color_component r,
                    float_color_component g,
                    float_color_component b);

void float_put_pixel_unsafe(float_image img,
                            unsigned int x,
                            unsigned int y,
                            float_color_component r,
                            float_color_component g,
                            float_color_component b);

void float_put_pixel_clip(float_image img,
                          unsigned int x,
                          unsigned int y,
                          float_color_component r,
                          float_color_component g,
                          float_color_component b);

float_grayimage float_alloc_grayimg(unsigned int, unsigned int);
float_grayimage float_tograyscale(float_image);
float_image float_tocolor(float_grayimage);
float_image float_get_ppm(FILE *pf);
void float_output_ppm(FILE *fd, float_image img);

/** INTEGER TYPES AND FUNCTIONS **/
typedef unsigned char color_component;
typedef color_component pixel[3];
typedef struct {
    unsigned int width;
    unsigned int height;
    pixel *buf;
} image_t;
typedef image_t * image;

typedef unsigned char luminance;
typedef luminance pixel1[1];
typedef struct {
   unsigned int width;
   unsigned int height;
   pixel1 *buf;
} grayimage_t;
typedef grayimage_t *grayimage;

image alloc_img(unsigned int width, unsigned int height);
void free_img(image);
void fill_img(image img,
        color_component r,
        color_component g,
        color_component b );

void put_pixel_unsafe(
       	image img,
        unsigned int x,
        unsigned int y,
        color_component r,
        color_component g,
        color_component b );

void put_pixel_clip(
       	image img,
        unsigned int x,
        unsigned int y,
        color_component r,
        color_component g,
        color_component b );

grayimage alloc_grayimg(unsigned int, unsigned int);

grayimage tograyscale(image);
image tocolor(grayimage);

image get_ppm(FILE *pf);
void output_ppm(FILE *fd, image img);

/** INTEGER/FLOAT CONVERSION **/
float_image integer_to_float_image(image integerImage);
image float_to_integer_image(float_image floatImage);

void integer_to_float_image_in_out(image integerImage, float_image floatImage);
void float_to_integer_image_in_out(float_image floatImage, image integerImage);
#endif