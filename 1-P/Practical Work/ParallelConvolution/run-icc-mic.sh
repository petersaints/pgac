#!/bin/sh
source /opt/intel/bin/iccvars.sh intel64
source /opt/intel/bin/compilervars.sh intel64
NUM_THREADS=(4 8 16 32 64 96 128 160 192 224 236)
NUM_RUNS=5
CONVOLUTION_TIME_LABEL="Convolution Time(ms) = "
RESULTS_FILE="results-icc-mic.csv"
export MIC_ENV_PREFIX=PHI
echo -n "" > "$RESULTS_FILE"
#Integer version
for i in "${NUM_THREADS[@]}"
do
	export PHI_OMP_NUM_THREADS=$i
	echo "Testing with $i threads"
	for j in $(seq 1 1 $NUM_RUNS)
	do
		RUN_TIME=$(./parallel-convolution-icc-mic plane.ppm out.ppm matrix.txt 2 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
		echo $RUN_TIME
		echo -n $RUN_TIME"," >> "$RESULTS_FILE"
	done
done
#Float version
echo "" >> "$RESULTS_FILE"
for i in "${NUM_THREADS[@]}"
do
	export PHI_OMP_NUM_THREADS=$i
	echo "Testing with $i threads"
	for j in $(seq 1 1 $NUM_RUNS)
	do
		RUN_TIME=$(./parallel-convolution-icc-mic plane.ppm out.ppm matrix.txt 3 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
		echo $RUN_TIME
		echo -n $RUN_TIME"," >> "$RESULTS_FILE"
	done
done
