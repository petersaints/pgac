#!/bin/sh
gcc imglib.c main.c -std=c99 -O3 -lm -fopenmp -o parallel-convolution-icc-opencl -D OPENCL_SUPPORT -I /opt/intel/opencl/include/ -L/opt/intel/opencl/lib64 -lOpenCL
