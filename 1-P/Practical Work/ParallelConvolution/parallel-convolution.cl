#pragma OPENCL EXTENSION cl_amd_printf : enable
#define CHANNELS 3

kernel void parallelconvolution(global unsigned char* inputImageBuffer,
                                global unsigned char* workingImageBuffer,
                                constant float *kernelFilter,
                                const uint2 inputImageSize,
                                const uint2 kernelFilterSize) {                                  
    const uint i = get_global_id(1);
    const uint j = get_global_id(0);

    const uint kCenterX = kernelFilterSize.x / 2;
    const uint kCenterY = kernelFilterSize.y / 2;

    const uint workingImageOffset = (inputImageSize.x * i + j) * CHANNELS;
    float3 sum = 0.0;
    for(int m=0; m < kernelFilterSize.y; m++) { //kernel rows
          int mm = kernelFilterSize.y - 1 - m; //row index of flipped kernel
          for(int n=0; n < kernelFilterSize.x; n++) { //kernel columns
              const int nn = kernelFilterSize.x - 1 - n;       //column index of flipped kernel
              //index of input signal, used for checking boundary
              int rowIndex = i + m - kCenterY;
              int colIndex = j + n - kCenterX;
              if(rowIndex >= 0 && rowIndex < inputImageSize.y && colIndex >= 0 && colIndex < inputImageSize.x) {
                  const int inputImageOffset = (inputImageSize.x * rowIndex + colIndex) * CHANNELS;
                  const int kernelOffset = kernelFilterSize.x * mm + nn;              
                  sum.x += inputImageBuffer[inputImageOffset+0] * kernelFilter[kernelOffset];
                  sum.y += inputImageBuffer[inputImageOffset+1] * kernelFilter[kernelOffset];
                  sum.z += inputImageBuffer[inputImageOffset+2] * kernelFilter[kernelOffset];
              }
          }
    }
    workingImageBuffer[workingImageOffset+0] = sum.x;
    workingImageBuffer[workingImageOffset+1] = sum.y;
    workingImageBuffer[workingImageOffset+2] = sum.z;
}