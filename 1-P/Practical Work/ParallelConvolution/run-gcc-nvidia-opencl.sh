#!/bin/sh
NUM_RUNS=5
CONVOLUTION_TIME_LABEL="Convolution Time(ms) = "
RESULTS_FILE="results-gcc-nvidia-opencl.csv"
echo -n "" > "$RESULTS_FILE"
#GPU Integer version on NVIDIA Tesla
for i in $(seq 1 1 $NUM_RUNS)
do
	RUN_TIME=$(./parallel-convolution-gcc-nvidia-opencl plane.ppm out.ppm matrix.txt 6 0 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
	echo $RUN_TIME
	echo -n $RUN_TIME"," >> "$RESULTS_FILE"
done
#GPU Float version on NVIDIA Tesla
echo "" >> "$RESULTS_FILE"
for i in $(seq 1 1 $NUM_RUNS)
do
	RUN_TIME=$(./parallel-convolution-gcc-nvidia-opencl plane.ppm out.ppm matrix.txt 7 0 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
	echo $RUN_TIME
	echo -n $RUN_TIME"," >> "$RESULTS_FILE"
done
echo "" >> "$RESULTS_FILE"
#GPU Integer version on NVIDIA Quadro
for i in $(seq 1 1 $NUM_RUNS)
do
        RUN_TIME=$(./parallel-convolution-gcc-nvidia-opencl plane.ppm out.ppm matrix.txt 6 1 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
        echo $RUN_TIME
        echo -n $RUN_TIME"," >> "$RESULTS_FILE"
done
#GPU Float version on NVIDIA Quadro
echo "" >> "$RESULTS_FILE"
for i in $(seq 1 1 $NUM_RUNS)
do
        RUN_TIME=$(./parallel-convolution-gcc-nvidia-opencl plane.ppm out.ppm matrix.txt 7 1 | grep "$CONVOLUTION_TIME_LABEL" | sed 's/.*= \(.*\)/\1/')
        echo $RUN_TIME
        echo -n $RUN_TIME"," >> "$RESULTS_FILE"
done

