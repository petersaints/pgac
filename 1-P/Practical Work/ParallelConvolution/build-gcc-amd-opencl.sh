#!/bin/sh
gcc imglib.c main.c -std=c99 -O3 -lm -fopenmp -o parallel-convolution-gcc-amd-opencl -D OPENCL_SUPPORT -I /opt/AMDAPP/include/ -L/opt/AMDAPP/lib/x86_64 -lOpenCL