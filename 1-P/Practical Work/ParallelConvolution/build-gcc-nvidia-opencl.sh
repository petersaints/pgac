#!/bin/sh
gcc imglib.c main.c -std=c99 -O3 -lm -fopenmp -o parallel-convolution-gcc-nvidia-opencl -D OPENCL_SUPPORT -I /usr/local/cuda/include/ -L/usr/local/cuda/lib64/ -lOpenCL
